up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear docker-build docker-up app-init

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

cli:
	docker-compose run --rm php-cli php bin/console

app-init: composer-install assets-install migrations

composer-install:
	docker-compose run --rm php-cli composer install

migrations:
	docker-compose run --rm php-cli php bin/console doctrine:migrations:migrate --no-interaction

fixtures:
	docker-compose run --rm php-cli php bin/console doctrine:fixtures:load --no-interaction

test:
	docker-compose run --rm php-cli php bin/phpunit

assets-install:
	docker-compose run --rm node yarn install
	docker-compose run --rm node npm rebuild node-sass --force

assets-dev:
	docker-compose run --rm node yarn run dev

assets-watch:
	docker-compose run --rm node yarn run watch
