<?php

namespace App\Tests\Builder;

use App\Entity\User;

class UserBuilder
{
    private $name;
    private $email;
    private $password;
    private $roles;
    private $status;
    private $verifyToken;

    public function __construct()
    {
        $this->name = 'name';
        $this->email = 'test@mail.ru';
        $this->password = 'tested';
        $this->roles = [User::ROLE_USER];
        $this->status = User::STATUS_WAIT;
        $this->verifyToken = \Faker\Factory::create()->uuid;
    }

    public function withRole(array $role): self
    {
        $clone = clone $this;
        $clone->roles = $role;
        return $clone;
    }

    public function build(): User
    {
        return (new User())
            ->setName($this->name)
            ->setEmail($this->email)
            ->setPassword($this->password)
            ->setRoles($this->roles)
            ->setStatus($this->status)
            ->setVerifyToken($this->verifyToken);
    }
}