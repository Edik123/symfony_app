<?php

namespace App\Tests\Unit;

use App\Tests\Builder\UserBuilder;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testRequest(): void
    {
        $user = (new UserBuilder())->build();

        self::assertNotEmpty($user);

        self::assertEquals('name', $user->getName());
        self::assertEquals('test@mail.ru', $user->getEmail());
        self::assertNotEmpty($user->getPassword());
        self::assertNotEquals('password', $user->getPassword());

        self::assertTrue($user->isWait());
        self::assertFalse($user->isActive());
        self::assertFalse($user->isAdmin());
    }
}
