<?php

namespace App\Tests\Unit;

use App\Entity\User;
use App\Tests\Builder\UserBuilder;
use PHPUnit\Framework\TestCase;

class RoleTest extends TestCase
{
    public function testChange(): void
    {
        $user = (new UserBuilder())->build();

        self::assertFalse($user->isAdmin());

        $user->changeRole(User::ROLE_ADMIN);

        self::assertTrue($user->isAdmin());
    }

    public function testAlready(): void
    {
        $user = (new UserBuilder())->withRole([User::ROLE_ADMIN])->build();

        $this->expectExceptionMessage('Role is already assigned.');

        $user->changeRole(User::ROLE_ADMIN);
    }
}
