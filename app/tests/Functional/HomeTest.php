<?php

namespace App\Tests\Functional;

use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeTest extends WebTestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $client = static::createClient();
        $response = $client->request('GET', '/');
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }
}
