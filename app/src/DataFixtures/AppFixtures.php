<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordEncoder;

    /**
     * @var \Faker\Factory
     */
    private $faker;

    public function __construct(UserPasswordHasherInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->faker = \Faker\Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $manager->flush();
    }

    public function loadUsers(ObjectManager $manager)
    {
        for ($i = 1; $i <= 10; $i++) {
            $user = new User();

            $active = $this->faker->boolean;
            $hash = $this->passwordEncoder->hashPassword($user, 'password');

            $user->setName($this->faker->name())
                ->setEmail($this->faker->unique()->safeEmail())
                ->setPassword($hash)
                ->setRoles($active ? $this->faker->randomElement([User::ROLE_USER, User::ROLE_ADMIN]) : [User::ROLE_USER])
                ->setRememberToken(substr(str_replace(['/', '+', '='], '', base64_encode(random_bytes(10))), 0, 10))
                ->setVerifyToken($active ? null : $this->faker->uuid)
                ->setStatus($active ? User::STATUS_ACTIVE : User::STATUS_WAIT);

            $manager->persist($user);
        }
    }
}