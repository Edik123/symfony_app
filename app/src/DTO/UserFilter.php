<?php
declare(strict_types=1);
namespace App\DTO;

class UserFilter
{
    public const STATUS_WAIT = 'Waiting';
    public const STATUS_ACTIVE = 'Active';

    public $id;
    public $name;
    public $email;
    public $status;
    public $roles;
}
