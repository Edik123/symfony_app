<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RoleCommand extends Command
{
    protected static $defaultName = 'app:user-role';
    protected static $defaultDescription = 'Set role for user';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        string $name = null,
        EntityManagerInterface $em,
        UserRepository $userRepository
    )
    {
        parent::__construct($name);
        $this->em = $em;
        $this->userRepository = $userRepository;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addOption('email', '', InputArgument::OPTIONAL, 'Email')
            ->addOption('role', '', InputArgument::OPTIONAL, 'Role')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getOption('email');
        $role = $input->getOption('role');

        $io->title('Change Role Command');
        $io->text([
            'Please, enter some information'
        ]);

        if (!$email) {
            $email = $io->ask('Email');
        }

        if (!$role) {
            $role = $io->ask('Set role');
        }

        $user = $this->userRepository->findOneBy(['email' => $email]);
        if (!$user) {
            $io->comment('Undefined user with email ' . $email);
            return Command::FAILURE;
        }

        try {
            $user->changeRole($role);
            $this->em->flush();
        } catch (\RuntimeException $e) {
            $io->comment($e->getMessage());
            return Command::FAILURE;
        }

        $io->success('Role is successfully changed');

        return Command::SUCCESS;
    }
}