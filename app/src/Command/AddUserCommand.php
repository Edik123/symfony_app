<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AddUserCommand extends Command
{
    protected static $defaultName = 'app:add-user';
    protected static $defaultDescription = 'Create user';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserPasswordHasherInterface
     */
    private $encoder;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        string $name = null,
        EntityManagerInterface $em,
        UserPasswordHasherInterface $encoder,
        UserRepository $userRepository
    )
    {
        parent::__construct($name);
        $this->em = $em;
        $this->encoder = $encoder;
        $this->userRepository = $userRepository;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addOption('email', '', InputArgument::OPTIONAL, 'Email')
            ->addOption('password', '', InputArgument::OPTIONAL, 'Password')
            ->addOption('role', '', InputArgument::OPTIONAL, 'Role')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $stopwatch = new Stopwatch();
        $stopwatch->start('add-user-command');

        $email = $input->getOption('email');
        $password = $input->getOption('password');
        $role = $input->getOption('role');

        $io->title('Add User Command');
        $io->text([
            'Please, enter some information'
        ]);

        if (!$email) {
            $email = $io->ask('Email');
            //$io->note(sprintf('You passed an argument: %s', $email));
        }

        if (!$password) {
            $password = $io->askHidden('Password (your type will be hidden)');
        }

        if (!$role) {
            $role = $io->ask('Set role');
        }

        try {
            $user = $this->createUser($email, $password, $role);
        } catch (RuntimeException $e) {
            $io->comment($e->getMessage());

            return Command::FAILURE;
        }

        $successMessage = sprintf('%s was successfully created: %s',
            $role,
            $email
        );
        $io->success($successMessage);

        $event = $stopwatch->stop('add-user-command');
        $stopwatchMessage = sprintf('New user\'s id: %s / Elapsed time: %.2f ms / Consumed memory: %.2f MB',
            $user->getId(),
            $event->getDuration(),
            $event->getMemory() / 1000000
        );
        $io->comment($stopwatchMessage);

        return Command::SUCCESS;
    }

    /**
     * @param string $email
     * @param string $password
     * @param string $role
     * @return User
     */
    private function createUser(string $email, string $password, string $role): User
    {
        $user = $this->userRepository->findOneBy(['email' => $email]);

        if ($user) {
            throw new RuntimeException('User already exist');
        }

        $user = new User();
        $user->setEmail($email);
        $user->setRoles([$role]);

        $encodedPassword = $this->encoder->hashPassword($user, $password);
        $user->setPassword($encodedPassword);

        $user->setVerifyToken(null);
        $user->setStatus(User::STATUS_ACTIVE);

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }
}
