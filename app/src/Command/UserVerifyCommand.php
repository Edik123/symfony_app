<?php

namespace App\Command;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UserVerifyCommand extends Command
{
    protected static $defaultName = 'user:verify';
    protected static $defaultDescription = 'Verify user email';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        string $name = null,
        EntityManagerInterface $em,
        UserRepository $userRepository)
    {
        parent::__construct($name);
        $this->em = $em;
        $this->userRepository = $userRepository;
    }

    protected function configure(): void
    {
        $this
            ->addOption('email', '', InputArgument::OPTIONAL, 'Email')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getOption('email');

        $io->title('User Verification Command');
        $io->text([
            'Please, enter some information'
        ]);

        if (!$email) {
            $email = $io->ask('Email');
        }

        $user = $this->userRepository->findOneBy(['email' => $email]);
        if (!$user) {
            $io->comment('Undefined user with email ' . $email);
            return Command::FAILURE;
        }

        try {
            $user->verify();
            $this->em->flush();
        } catch (\RuntimeException $e) {
            $io->comment($e->getMessage());
            return Command::FAILURE;
        }

        $io->success('User is successfully verified');

        return Command::SUCCESS;
    }
}
